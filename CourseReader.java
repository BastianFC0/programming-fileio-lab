import java.util.List;
import java.util.ArrayList;
import java.io.IOException;
import java.nio.*;
import java.nio.file.*;

public class CourseReader {
    public static List<Meeting> getCourses(String file) throws IOException{
        Path path = Paths.get(file);
        List<String> list = Files.readAllLines(path);
        ArrayList<Meeting> meeting = new ArrayList<Meeting>();
        for(String line:list){
            String[] shard = line.split(",");
            if(shard[0].equals("inperson")){
                meeting.add(new InPerson(shard[0], shard[1], shard[2], shard[3], shard[4]));
            }
            if(shard[0].equals("online")){
                meeting.add(new Online(shard[0], shard[1], shard[2], shard[3]));
            }
        }
            return meeting;
        }
        
public static void main(String[] args) throws IOException{
    List<Meeting> meeting = getCourses("courses.csv");
    System.out.println(meeting.get(1).title);
    }  
}
