public class Meeting{
    protected String type;
    protected String title;
    public Meeting(String type, String title){
        this.type = type;
        this.title = title;
    }
    public String getType(){
        return this.type;
    }
    public String getTitle(){
        return this.title;
    }
}