public class Group extends Meeting{
    private String room;
    public Group(String type, String title, String room){
        super(type, title);
        this.room = room;
    }
    public String getRoom(){
        return this.room;
    }
}
