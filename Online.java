public class Online extends Meeting {
    protected String teacher;
    protected String classs;
    public Online(String type, String title, String teacher, String classs){
        super(type, title);
        this.teacher = teacher;
        this.classs = classs;
    }
    public String getTeacher(){
        return this.teacher;
    }
    public String getClasss(){
        return this.classs;
    }
}
