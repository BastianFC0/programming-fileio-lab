public class InPerson extends Online {
    private String room;
    public InPerson(String type, String title, String teacher, String classs, String room){
        super(type, title, teacher, classs);
        this.room = room;
    }
    public String getRoom(){
        return this.room;
    }
}
